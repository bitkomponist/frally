const ncp = require('ncp');
const {join} = require('path');
const {existsSync} = require('fs');
const {argv} = require('yargs')
  .alias('w','which')
  .default('w',join(__dirname,'../project-boilerplates/default'));

const POST_CREATE_FILE = '.frally.postcreate.js';

module.exports = function(gulp,config){
  const {which} = argv;
  gulp.task('boilerplate',(complete)=>{
    const dir = process.cwd();
    ncp(
      which,dir,{
        clobber:false,
      },function(err){
        if(err) throw err;
        const postFile = join(dir,POST_CREATE_FILE);
        if(existsSync(postFile)){
          
          delete require.cache[require.resolve(postFile)]
          require(postFile)();
          
          complete();
        }
      }
    );
  });
}
