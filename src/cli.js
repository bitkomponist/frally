
const {resolve,isAbsolute} = require('path');
const {existsSync} = require('fs');

const args = require('yargs')
        .alias('c','config')
        .argv
    ;

function getConfig(absOrRelPath) {
  const configFile = isAbsolute(absOrRelPath)?absOrRelPath:resolve(process.cwd(),absOrRelPath);

  if(!existsSync(configFile)){
    console.warn(`config file will be ignored since it is not readable (${configFile})`);
    return {};
  }

  return require(configFile);
}

const frally = require('./frally')(getConfig(args.config || 'frally.config.js'));
frally.runTask(args._[0]);
