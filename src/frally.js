const gulp = require('gulp');
const gulpUtil = require('gulp-util');
const {argv} = require('yargs');
const ENV_LIST = ["development","production"];
const extend = require('extend');

function getEnv(def=ENV_LIST[0]){
    for(let env of ENV_LIST)
        if(env in argv) return env;

    if(ENV_LIST.includes(process.env.NODE_ENV)){
        return process.env.NODE_ENV;
    }

    return def;
}

function getEnvConfig(config, env=getEnv()){
    if(env in config){
        return extend(true,{},config,config[env]);
    }
    return config;
}


module.exports = function frally(config = {}){
    //load Tasks with config
    config = getEnvConfig(config);

    const tasksRegistry = {};
    function register(module){
      extend(true,tasksRegistry,module(gulp,config));
    }


    // hier werden die unter tasks geladen
    // die sollen dann in die registry geschrieben werden, also z.B.
    // { build: {build-js:true}} zurück geben, damit dann der gruppentask "build" alle registrierten tasks ausführt.
    if(config.use){
        config.use.forEach((moduleOrName)=>{
            const module = typeof(moduleOrName) === 'string' ? require(moduleOrName) : moduleOrName;
            if(typeof(module) === 'object'){
              Object.keys(module).map((m)=>register(module[m]));
            }else{
              register(module);
            }
        });
    }

    register(require('./boilerplate'));

    Object.keys(tasksRegistry).forEach((groupTask)=>{
        gulp.task(groupTask,Object.keys(tasksRegistry[groupTask]));
    });

    return {
        runTask(name="default"){
            if(gulp.hasTask(name)){
                gulp.start(name);
            }
        }
    };
};
