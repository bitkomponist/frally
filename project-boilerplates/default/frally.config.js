const essentials = require('frally-essentials');
const {join} = require('path');
const cssnano = require('cssnano');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');
const {optimize,DefinePlugin} = webpack;
const {AggressiveMergingPlugin,DedupePlugin,UglifyJsPlugin} = optimize;
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');
const browsers = ["> 1%","last 2 versions","IE 11"];
const {existsSync} = require('fs');
const ftp = existsSync(join(__dirname,'deploy.json'))?require('./deploy.json'):{};

module.exports = {
    use:[essentials],
    html:{
        entry:'src/pug/views/**/*.pug',
        watch:['src/pug/**/*.{pug,js}',join(__dirname,'data/**/*.*')],
        basedir:join(__dirname,'src/pug'),
        output:'build',
        options:{
            locals:join(__dirname,'./data/index.js'),
        }
    },
    css:{
        entry:'src/scss/main.scss',
        watch:'src/scss/**/*.scss',
        output:'build/css',
        sass:{
            includePaths:[
                join(__dirname,'node_modules'),
            ]
        }
    },
    assets:{
        entry:'assets/**/*.{png,gif,jpg,jpeg,svg}',
        watch:'assets/**/*.{png,gif,jpg,jpeg,svg}',
        copy:'assets/**/!(*.png|*.gif|*.jpg|*.jpeg|*.svg)',
        output:'build'
    },
    js:{
        entry: {
            main:join(__dirname,'src/js/main.js'),
        },
        output:{
            path: join(__dirname,"build/js"),
            filename: "[name].js",
            publicPath: 'js',
        }
    },
    development:{
        js:{
            module: {
                loaders: [
                    {
                        test: /\.(js|jsx)$/,
                        exclude: /(node_modules|bower_components)/,
                        loader: 'babel-loader',
                        query: {
                            presets: [['env',{modules:false,targets:{browsers},debug:true}]]
                        }
                    }
                ]
            },
            plugins:[
                new BundleAnalyzerPlugin(),
                new DefinePlugin({
                  'process.env': {
                    'NODE_ENV': JSON.stringify('development')
                  }
                }),
            ],
            devtool: "source-map",
            devServer: {
                publicPath:'/js',
                contentBase: join(__dirname, "build"),
                inline: true,
                hot:true,
                port:8080,
                host:'localhost',
                open:true,
            },
        },
        css:{
            postcss:{
                plugins:[
                    autoprefixer({browsers}),
                ],
            }
        },
    },
    production:{
        deploy:{
            entry:'build/**/*.*',
            base:'build',
            output: '/',
            ftp
        },
        js:{
            module: {
                loaders: [
                    {
                        test: /\.(js|jsx)$/,
                        exclude: /(node_modules|bower_components)/,
                        loader: 'babel-loader',
                        query: {
                            presets: [['env',{modules:false,targets:{browsers},debug:false}]]
                        }
                    }
                ]
            },
            plugins:[
                new UglifyJsPlugin(),
                new DefinePlugin({
                  'process.env': {
                    'NODE_ENV': JSON.stringify('production')
                  }
                }),
            ],
        },
        css:{
            postcss:{
                plugins:[
                    autoprefixer({browsers}),
                    cssnano({
                        reduceIdents: false,
                        safe:true
                    })
                ],
            }
        }
    }
};
